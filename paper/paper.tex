\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{textcomp}
\usepackage{newfloat}
\usepackage{graphicx}
\usepackage{tipa}

\addtolength{\oddsidemargin}{-.875in}
\addtolength{\evensidemargin}{-.875in}
\addtolength{\textwidth}{1.75in}
\addtolength{\topmargin}{-.875in}
\addtolength{\textheight}{1.75in}
\linespread{1.5}
\begin{document}

\title{IPA Search Puzzles and Phonotactics}
\date{May 12th, 2014}
\author{Aaron Free and Nick Hammes \\* Phonology 1 Final Paper \\* Linguistics Department, University of Minnesota}
\maketitle
\newpage

\tableofcontents
\newpage

\section{Introduction}
From the first time we were introduced to IPA search puzzles, as Computer Science majors who have spent their academic careers building computational systems piece by piece in order to reused, we were intrigued by the phonotactic nature of these puzzles. What sorts of phonological combinations could a puzzle consist of? What constitutes an interesting and/or difficult IPA search puzzle? How would one build a system that would be able to generate IPA search puzzles for a given language? Throughout this paper, we will explore how we built our own IPA search puzzles, what we learned from a phonological standpoint about how people solve these puzzles from our own experimentation, and how we believe we can use this phonological knowledge to create optimally hard IPA search puzzles.

\section{Building an IPA search puzzle from the bottom up}
Our ambitions to reverse engineer the IPA search puzzle had to start somewhere, and often times, in programming, that involves reducing the problem down to it’s simplest parts and iteratively abstracting it further and further until the problem is solved. For our IPA search puzzles, our basic atomic units were phonemes and as such, we decided the best first step in building our puzzle generator is to find a way to represent phonemes computationally. In Python, our programming language of choice, there simply is not a good way to do this out of the box. However, Python does contain a basic data structure, called a dictionary, which allows a user to relate two objects to each other, analogous to how a real dictionary maps a word to its definition, as well as the unicode character representations of each IPA character. As such, we leveraged this machinery to map IPA unicode characters to the lists of their feature representations and vice versa.  We decided to use the American English subset of IPA as a starting point, and our finalized American English phoneme-feature dictionary consisted of a set of IPA characters and feature values from a list we found that created by the a professor member at UMass, which can be found at
http://blogs.umass.edu/eba/files/2012/01/201Spring-IPA-Feature-Chart3.pdf.

From here, we had encoded our own representations of American English IPA, but how could we abstract further to relate words in our language to their proper pronunciations, in order to create and validate our puzzle solutions? We did some deep searching for this problem, but we found out that, at least for the English language, there exists a pronouncing dictionary, which was already implemented in Python through the Natural Language Toolkit programming library. There was just one major road block with this method, as the CMUDict uses the ARPABET format, rather than the IPA format we desired to represent phonemes, so we created a function that maps the whole pronouncing dictionary from the similar but not exactly one-to-one related ARPABET format.
From this point, creating our own IPA puzzles was relatively easy. The necessary components needed for writing puzzles to files, creating the puzzle matrices and filling them phonemes and their proper solutions sets were already contained within Python and readily available to us. However, we realized that simply generating the puzzles in a purely naive fashion would not make for an interesting enough Phonology I final project, so we decided it would be necessary to conduct an experiment to gain some insight into the linguistics behind solving these puzzles, and how we could make them variable in difficulty given this knowledge.

\section{The Experiment}
Our experiment consisted of generating 8 unique IPA search puzzles, ordinally named after the first 8 letters of the Greek alphabet. Each member of our Phonology I class received a randomly selected copy of 2 of these puzzles. Each puzzle consisted of a 20x20 matrix at our baseline difficulty, and each participant in our study was given 4 minutes to solve each puzzle. Our rationale for this was that we could find phonological patterns in our data, and controlling for time, dimensions and difficulty would decrease the possibilities of bias in our data, although we unfortunately did need to throw out puzzle Beta from our data set after the experiment, as it was malformed.  

\section{Results}

\begin{figure}[h]
\centering
\includegraphics{results.png}
\caption{Mean words found per puzzle.}
\end{figure}

Our results showed a particularly interesting pattern. As Figure 1 shows, one of the puzzles showed a particularly higher level of difficulty (Puzzle delta) and two other puzzles (Puzzles zeta and eta) showed a lower than average number of words found.  Based on these puzzles, we found that our easiest measure of difficulty was predictable by counting the number of valid syllables around a particular phoneme in a word. This is because in a word like pithy [p\textipa{I}\textipa{T}i], with its nearest neighboring phonemes consisting of w’s and m’s in one of the example puzzles, were more likely to be solved in these contexts because the [\textipa{T}i] syllable is not very likely to be found near an m or a w given the target word, and this was the only word in the puzzle that contained this syllable, so it was easy to disambiguate the location of the [pI] syllable in this context. 

The theory of word recognition based on clustering phonemes into syllables has also been observed by phonologists in the past. In Michael S Vivevitch, and Paul A. Luce’s, paper Probabilistic Phonotactics and Neighborhood Activation in Spoken Word Recognition, they implement an experiment to test the recognition of syllables by giving participants a list of both nonsense and valid American English words, and were told to see how long it would take people to react to these words. Their results showed that simple syllables with more commonly used patterns such as [s\textipa{2}v] with [s] being a common onset and [v] being a common coda in American English, as well as [s\textipa{2}]  and [\textipa{2}v] being commonly occurring biphone pairs,  were more likely to be recognized and had a lower reaction time among participants, while low probability syllables with less commonly occurring phonemes or phoneme patterns such as the biphone cluster [a\textipa{O}]. Thus, the results of their experiment in combination with what we found in our own experimentation gives rise to the idea that as people solve a puzzle, they first look for syllable clusters of high probability contained in a particular word, and then work to reject the low probability syllable clusters for this word around a high probability cluster in order to reduce the number of possible syllable clusters that could be contained until they have found their solution.

\section{Constructing a Gradient of Difficulty}
Given our results, we decided to make 3 difficulty measures, ranging to from what we believe is randomly hard to optimally hard, which should gradiently take more and more time for a participant to solve by creating increasingly less obvious syllable clusters for each difficulty. As our default random difficulty, difficulty one places random IPA characters in the search matrix after all the solutions have been filled in correctly. This allows the puzzle to have a definite base line since it does not do any work towards correcting possibly obvious syllable clusters in the puzzle. 

Difficulty 2, however, takes a second pass at the matrix after this random fill, randomly changing IPA characters in the matrix before it is returned to the user. The idea behind this process is that it will naively break up easy to spot syllable patterns in the puzzle that would normally appear in a difficulty one puzzle, and thus the puzzle will take more time to solve.

	Our difficulty 3 puzzle, however, takes a more intelligent approach than the previous two difficulties. Because difficulty is proportional to the number of pronounceable syllables, we can actually represent the problem as a search tree of grids, where each child node has one more character filled in than its parent, and has a heuristic “score” equal to the number of pronounceable syllables on the entire grid. Difficulty 3, once implemented, will search through this tree, and using various Artificial Intelligence principles, we are able to ensure we find the grid with the largest possible heuristic score, given the words we’ve inserted. Given the difficulty predictions gleaned from our data, this should produce an optimally difficult puzzle.

\section{Conclusion}
In summary, we believe that, given our research, we have truly created an algorithm to generate the optimally difficult IPA search puzzle. Our ground-up approach allowed us to build our puzzle generator quickly and keep iteratively tweaking it as we learned more and more about these puzzles. Given the results of our experiment, we found that by breaking up more predictable syllable clusters, we can leverage the previous phonotactic research done by linguists and increase the complexity of an IPA puzzle. Other possible steps for the future of our IPA search puzzle generator include implementing our optimally difficult puzzle generating algorithm as well as our middle difficulty search puzzle generating algorithm, an extension for turning the puzzle generator into a web application, adding support for different levels of stress in syllables and extending the programming to generate puzzles for IPA language subsets other than American English, given a pronouncing dictionary for the desired language, to create verifiable solutions. Nevertheless, we hope our IPA search puzzle generator is useful regardless, and enables users to generate lots of fun and difficult puzzles.

\section{Bibliography}

“The International Phonetic Alphabet (IPA)”
http://blogs.umass.edu/eba/files/2012/01/201Spring-IPA-Feature-Chart3.pdf

Vitevitch, Michael S., and Paul A. Luce. "Probabilistic Phonotactics and
Neighborhood Activation in Spoken Word Recognition." (1999): 380-82. Web. 12 May 2014. <http://129.237.66.221/V%26L99.pdf>.
\end{document}
