#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'nick hammes and aaron free'
# Python Standard Module imports
from optparse import OptionParser, OptionGroup
from random import randrange
import sys

# Python Dependency Module imports

# Custom Module imports
from Puzzle import Puzzle
from Language import Language
from Crossword import Crossword


def main():
    #Parse and deal with command line input
    parser = OptionParser()
    group = OptionGroup(parser, "Input Options")
    group.add_option("-d", "--dimensions", action="store", type="string",
                     help="Specify dimensions of puzzle output.")
    group.add_option("-l", "--language", action="store", type="string",
                     help="Specify puzzle language.")
    group.add_option("--difficulty", action="store", type="string",
                     help="Specify puzzle difficulty on a scale of 1-3.")
    group.add_option("--languagepath", action="store", type="string",
                     help="Specify non-standard location of language file.")
    group.add_option("-r", "--root", action="store", type="string",
                     help="Point to non-standard directory, word search generator root.")
    group.add_option("-w", "--wordcount", action="store", type="string",
                     help="How many words to insert into the word search.")
    parser.add_option_group(group)
    group = OptionGroup(parser, "Output Options")
    group.add_option("--txt", action="store", help="Write finished puzzle to a text document.")
    group.add_option("--latex", action="store", help="Write finished puzzle to a LaTeX document.")
    parser.add_option_group(group)
    parser.add_option("-D", "--debug", action="store_true", default=False, help="Print all debug information.")
    (options, args) = parser.parse_args()
    # Sanitize input
    if options.difficulty is None:
        sys.stderr.write("[Warning] No difficulty specified. Defaulting to 1.\n")
        options.difficulty = '1'
    if options.language is None:
        print "[Error] No language specified. Cannot recover, exiting."
        parser.print_help()
        return -2
    # Generate Word Search puzzle
    wordSearch = Puzzle(options.dimensions)
    generateWordSearch(wordSearch, options.language, debug=options.debug,
                       difficulty=options.difficulty, wordcount=int(options.wordcount))
    # Output puzzle formatted per requested method
    if options.txt:
        wordSearch.writeTXT(outfile=options.txt)
        print "[Message] Written to " + options.txt
    if options.latex:
        wordSearch.writeLaTex(outfile=options.latex)
        print "[Message] Written to " + options.latex
    if not options.latex and not options.txt:
        wordSearch.printC()
    return 0


def generateWordSearch(puzzle, language, debug=False, difficulty='1', wordcount=20):
    # Create complex data structures
    lang = Language(language, "English", language)
    puzzle_list = []
    newwords = []
    # Pick words to insert into puzzle
    words = lang.getRandomVocabulary(wordcount)
    if debug:
        print words
    for word in words:
        y = [x.strip(unichr(809)) for x in word[0]]
        x = ""
        for letter in y:
            x += letter
        word = (x, word[0:])
        if randrange(0, 1, 1):
            word = (word[0][::-1], word[0:])
        newwords.append(word)

    # Insert words into puzzle
    cword = Crossword(puzzle.xdim, puzzle.ydim, '-', 5000, newwords)
    cword.compute_crossword()
    lines = cword.solution()
    for line in lines.split("\n"):
        x = ((line.split(" ")))
        puzzle_list.append(x[:-1])
    puzzle_list = puzzle_list[:-1]
    if debug:
        print len(puzzle_list)
        print len(puzzle_list[0])
    puzzle.loadPuzzleList(puzzle_list)
    if debug:
        puzzle.printC()
    # Insert non-words into puzzle
    fillReturn = puzzle.fillPuzzle(difficulty=difficulty, language=lang)
    if fillReturn is not 0:
        return -1
    puzzle.words = words
    if debug:
        print "Post-fill"
        puzzle.printC()
        print "Words:"
        for word in words:
            print word[1]
    # Clean up

    return puzzle

main()
