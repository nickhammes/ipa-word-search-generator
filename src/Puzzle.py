# -*- coding: utf-8 -*-
import cPickle as pickle
import random
import os
import sys

from Phoneme import Phoneme


class Puzzle:
    def __init__(self, dimensions, words = ""):
        (xdimension, ydimension) = dimensions.split('x')
        self.xdim = int(xdimension)
        self.ydim = int(ydimension)
        self.data = []
        self.hitmap = []
        self.words = words
        try:
            self.deffeat = \
                pickle.load(open(os.path.dirname(os.path.realpath(sys.argv[0])) + '/../data/ipa.pickle', 'rb'))
        except:
            print "Unable to load ipa.pickle from data directory. Exiting."
            raise
        for x in range(0, self.xdim):
            self.data.append([])
            self.hitmap.append([])
            for y in range(0, self.ydim):
                self.data[x].append(Phoneme('', features = []))
                self.hitmap[x].append(0)
        return

    #Print IPA Characters of Puzzle
    # Returns void
    def printC(self):
        for i in range(0, self.xdim):
            for j in range(0, self.ydim):
                print(self.data[i][j].getChar() + "\t"),
            print "\n"
        return

    # Print Hitmap of Puzzle
    # Returns void
    def printH(self):
        for i in self.hitmap:
            print i
        return

    def loadPuzzleList(self, puzzleList):
        for i in range(0, len(self.data) - 1):
            for j in range(0, len(self.data[i]) - 1):
                #self.data[i][j].char = puzzleList[i][j]
                if puzzleList[i][j] == u'ł':
                    self.data[i][j].char = u'l'
                else:
                    self.data[i][j].char = puzzleList[i][j]
                if not (self.data[i][j].char == "-"):
                    self.hitmap[i][j] = 1
        return

    # Adds feature to (xcoord, ycoord) if it is modifiable
    # Returns 1 on success
    # Returns 0 on failure
    def addFeatureLocation(self, xcoord, ycoord, feature):
        if self.hitmap[xcoord][ycoord] == 0:
            self.data[xcoord][ycoord].addFeature(feature)
            return 1
        else:
            return 0

    # Sets character at (xcoord, ycoord) if it is modifiable
    # Marks character unmodifiable
    # Returns 1 on success
    # Returns 0 on failure
    def addCharacterLocation(self, xcoord, ycoord, character):
        if self.hitmap[xcoord][ycoord] != 1:
            self.data[xcoord][ycoord].setChar(character)
            self.hitmap[xcoord][ycoord] = 1
            return 1
        else:
            return 0

    # Writes IPA Characters of Puzzle into .tex file
    # Returns null
    # Todo: Handle IO Errors, write location
    def writeLaTex(self, outfile = 'puzzle.tex'):
        """Writes the LaTex file of the finalized puzzle."""
        latex_file = open(outfile, 'w')
        latex_file.write("""\\documentclass[12pt]{article}
\\usepackage[utf8]{inputenc}
\\usepackage{textcomp}
\\usepackage{newfloat}
\\usepackage{tipa}

\\addtolength{\\oddsidemargin}{-.875in}
\\addtolength{\\evensidemargin}{-.875in}
\\addtolength{\\textwidth}{1.75in}
\\addtolength{\\topmargin}{-.875in}
\\addtolength{\\textheight}{1.75in}
\\linespread{1.5}
\\begin{document}
""")
        latex_file.write("""
\\begin{center}
\\begin{tabular}{""")
        for i in xrange(0, self.xdim):
            latex_file.write("c ")
        latex_file.write("""}

        """)
        for x in self.data:
            for y in range(0, len(x)):
                #print y.char
                if len(x[y].char) == 1:
                    if ord(x[y].char) <= 128:  # ASCII characters printed as-is
                        latex_file.write(x[y].char + "")
                    elif x[y].char == unichr(643):  # S function curve
                        latex_file.write("\\textipa{S}")
                    elif x[y].char == unichr(601):  # Schwa
                        latex_file.write("\\textipa{@}")
                    elif x[y].char == unichr(618):  # Small Cap I
                        latex_file.write("\\textipa{I}")
                    elif x[y].char == unichr(658):  # Hard G ligature
                        latex_file.write("\\textipa{Z}")
                    elif x[y].char == unichr(660):  # Glottal Stop
                        latex_file.write("\\textipa{P}")
                    elif x[y].char == unichr(596):  # Backwards C
                        latex_file.write("\\textipa{0}")
                    elif x[y].char == unichr(603):  # Epsilon
                        latex_file.write("\\textipa{3}")
                    elif x[y].char == unichr(952):  # Theta
                        latex_file.write("\\textipa{T}")
                    elif x[y].char == unichr(650):  # Upsilon
                        latex_file.write("\\textipa{U}")
                    elif x[y].char == unichr(633):  # Rhotic R
                        latex_file.write("\\textturnlonglegr")
                    elif x[y].char == unichr(240):  # Th
                        latex_file.write("\\textipa{D}")
                    elif x[y].char == unichr(331):  # Engma
                        latex_file.write("\\textipa{N}")
                    elif x[y].char == unichr(619):  # Dark L
                        latex_file.write("l") # Kill Dark L
                    elif x[y].char == unichr(653):  # Turned W
                        latex_file.write("\\*w")
                    elif x[y].char == unichr(230):  # Ash Ligature
                        latex_file.write("\\ae")
                    elif x[y].char == unichr(652):  # Karet Mark
                        latex_file.write("\\textipa{2}")
                    else:
                        print "[single length character] " + x[y].char
                else:
                    if x[y].char == "d" + unichr(658):  # dG
                        latex_file.write("d\\textipa{Z}")
                    elif x[y].char == "a" + unichr(650):  # aU
                        latex_file.write("a\\textipa{U}")
                    elif x[y].char == "a" + unichr(618):  # aI
                        latex_file.write("a\\textipa{I}")
                    elif x[y].char == unichr(596) + unichr(618):  # cI
                        latex_file.write("\\textipa{O}\\textipa{I}")
                    elif x[y].char == "t" + unichr(643):  # tf
                        latex_file.write("t\\textipa{S}")
                    else:  # this should be unreachable, for AmEng
                        print "[double length character] " + x[y].char
                if y == len(x) - 1:
                    latex_file.write(" \\\\")
                else:
                    latex_file.write(" & ")
            latex_file.write("\n")
        latex_file.write("""\\end{tabular}

Vocabulary: \\\\*

\\begin{tabular}{c c}
""")

        for x in range(0, len(self.words),2):
            if x < len(self.words) - 1:
                latex_file.write(self.words[x][1] + " & " + self.words[x+1][1] + "\\\\\n")
            else:
                latex_file.write(self.words[x][1] + " & " + "\\\\\n")

        latex_file.write("""
\end{tabular}
\end{center}
\\end{document}""")

        # Todo: write a key the correlates to our puzzle matrix.
        return

    # Wrapper Function for Puzzle Fill Algorithms
    # Returns 0 on success
    # Returns nonzero on failure
    def fillPuzzle(self, language, difficulty = 1):
        retval = 0
        if difficulty == '1':
            retval = self.randomFill()
        elif difficulty == '2':
            retval = self.randomFeatureFill()
        elif difficulty == '3':
            retval = self.hillClimbingFill()
        else:
            print "[Error] Difficulty value given invalid. Cannot continue"
            return -1
        return retval

    def randomFill(self):
        # Determine characterset for language
        chars = []
        chars = self.deffeat.values()
        try:
            chars.remove("")
        except ValueError:
            pass
        # Loop over self.data
        for x in range(0, self.xdim):
            for y in range(0, self.ydim):
                self.addCharacterLocation(x, y,chars[random.randrange(0,len(chars) - 1, 1)])
        # insert random character in place
        return 0

    def randomFeatureFill(self,seed = random.randint):
        self.randomFill(seed)
        print "[Error] Diffculty level not yet implemented, please try again with a different difficulty level."
        sys.exit(0)
        return 0

    def hillClimbingFill(self):
        print "[Error] Diffculty level not yet implemented, please try again with a different difficulty level."
        sys.exit(0)
        return 0

    def ruleFill(self):
        #find all x characters.
        #[+alveolar] -> [+dental]
        # for : for : if self.data[][].hasFeature('dental')
        # add dental for near x.
        # if self.data.hasFeature('nasal') and !self.data.hasFeature('nasal') near
        # assimilate nasal place.
        chars = []
        chars = self.deffeat.values()
        chars.remove("")
        temphitmap = self.hitmap

        for x in range(0,self.xdim):
            for y in range(0,self.ydim):
                self.addCharacterLocation(x,y,chars[random.randrange(0,len(chars) - 1,1)])

        # nasal place assim
        for i in range(random.randrange(self.xdim*self.ydim)):
            tempx = random.randrange(self.xdim)
            tempy = random.randrange(self.ydim)
            if temphitmap[tempx][tempy] == 0:
                if self.data[tempx][tempy].hasFeature('nasal'):
                    for x in tempx: #?
                        for y in tempy: #?
                            if not (self.data[x][y].hasFeature('vocalic')) and not (self.data[x][y].hasFeature('nasal')):
                                self.data[x][y].addFeature('nasal',True)
        return 0

    def writeTXT(self, outfile = 'puzzle.txt'):
        try:
            os.stat(outfile)
        except OSError:
            outfile = os.getcwd() + "/" + outfile
        out = open(outfile, 'w')
        for i in range(0, self.xdim):
            for j in range(0, self.ydim):
                out.write((self.data[i][j].getChar() + "\t").encode('utf8'))
            out.write("\n")
        for word in self.words:
            out.write(word[1] + "\n")
        return