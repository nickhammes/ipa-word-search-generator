import cPickle as pickle
from random import randrange
import os
import sys


class Language:
    def __init__(self, Name, vocabulary):
        self.name = Name
        try:
            os.stat(vocabulary)
        except OSError:
            vocabulary = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../data/" + vocabulary
        try:
            self.vocabulary = pickle.load(open(vocabulary, "rb"))
        except:
            print "[Error] Unable to load dictionary at " + vocabulary + ". Please try again with a valid path."


    #def getRules(self):
        #return self.rules

    def getRandomVocabulary(self, quantity=1):
        ret = []
        for i in xrange(0, quantity):
            ret.append(self.vocabulary[randrange(0, len(self.vocabulary))])
        return ret
