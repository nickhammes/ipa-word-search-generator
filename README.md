Welcome to the IPA word search repository. Here you will find all the files needed to run generate the IPA search puzzles. 

Installion
	You will need Python 2.7, the NLTK, and tipa as well if you want LaTex writing support. Please make sure you have these things before using this script.

Usage:
	You can run the IPA search generator with: 


python main.py -d="rowdimension"x"columndimension" --difficulty="difficultyonascaleof1-3" -l="pathtolanguagefile" --txt="textfilename" or --latex="latexfilename"


where you fill in the relevant values where the quotes are. More info is listed below.

     -D, --debug           Print all debug information.

  Input Options:
    
    -d DIMENSIONS, --dimensions=DIMENSIONS
                        Specify dimensions of puzzle output.
    -l LANGUAGE, --language=LANGUAGE
                        Specify puzzle language.
    --difficulty=DIFFICULTY
                        Specify puzzle difficulty on a scale of 1-3.
    --languagepath=LANGUAGEPATH
                        Specify non-standard location of language file.
    -r ROOT, --root=ROOT
                        Point to non-standard directory, word search generator
                        root.
    -w WORDCOUNT, --wordcount=WORDCOUNT
                        How many words to insert into the word search.

  Output Options:
    
    --txt=TXT           Write finished puzzle to a text document.
    --latex=LATEX       Write finished puzzle to a LaTeX document.

    -h,--help           Diplays this useage information.
	
