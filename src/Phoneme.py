__author__ = 'nick hammes and aaron free'

#import cPickle as pickle

# A Phoneme is a single discrete unit of sound
# with some number of features, and represented
# by a single International Phonetic Alphabet
# character. The features uniquely describe the
# sound, and the character encodes it.

# Features are represented as an unordered list.


class Phoneme:
    def __init__(self, char, features):
        self.char = char
        self.features = features

    def getFeatures(self):
        return sorted([ft[0] for ft in self.features])

    def getChar(self):
        return self.char

    def setChar(self, char):
        self.char = char
        return

    def printFeature(self, featurename):
        for ft in self.features:
            if ft[0] is featurename:
                if ft[1] is True:
                    print "+" + ft[0]
                elif ft[1] is False:
                    print "-" + ft[0]
                else:
                    print ft[0]

    def printFeatures(self):
        for ft in self.features:
            if ft[1] is True:
                print "+" + ft[0]
            elif ft[1] is False:
                print "-" + ft[0]
            else:
                print ft[0]

    def hasFeature(self, feature):
        for ft in self.features:
            if (ft[0] == feature) and (ft[1]):
                return True

        return False

    def featureExists(self, feature):
        for ft in self.features:
            if (ft[0] == feature) and ft[1] is True or ft[1] is False:
                return True

        return False

    def addFeature(self, feature, value=True):
        #Check if feature is valid
        if self.featureExists(feature):
            templist = [i for i in self.features if i[0] != feature]
            templist.append((feature, value))
            self.features = sorted(templist)       

        if not (self.hasFeature(feature)):
            return

    def removeFeature(self, feature):
        self.features = sorted([x for x in self.features if x[0] is not feature])

    def findChar(self):
        """ This function should calculate an IPA character for the features included,
        if there is one unique character."""
        # It should return an error code otherwise.
        char = self.ipadict[repr(sorted(self.features))]
        #
        #
        if char is None:
            return u"INVALID FEATURE SET"
        else:
            return unicode(char) 
