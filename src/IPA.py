# -*- coding: utf-8 -*-
import Phoneme
import pickle




class IPA:
    self.ipadict = {}

    def __init__(self):
        self.ipadict = pickle.load(open('ipa.pickle','rb')) 

    def getIPAcharacters(self):
        return [i for i in ipadict.values()]       

    def getIPAcharacter(self, Phoneme):
        return unicode(ipadict[repr(Phoneme.features)])
    
    def update(self,character,features):
        self.ipadict.update({repr(features):unicode(character)})
        pickl = open('ipa.p','wb')
        pickle.dump(self.ipadict,pickl)

